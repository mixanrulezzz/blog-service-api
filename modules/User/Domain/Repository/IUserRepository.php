<?php

namespace Modules\User\Domain\Repository;

use Modules\User\Domain\Entity\UserEntity;
use Modules\User\Domain\ValueObject\UserEmail;
use Modules\User\Domain\ValueObject\UserId;

/**
 * Интерфейс для репозитория пользователя
 */
interface IUserRepository
{
    /**
     * Получить пользователя по идентификатору пользователя
     * @param UserId $userId
     * @return UserEntity|null
     */
    public function getById(UserId $userId): ?UserEntity;

    /**
     * Получить пользователя по email
     * @param UserEmail $userEmail
     * @return UserEntity|null
     */
    public function getByEmail(UserEmail $userEmail): ?UserEntity;

    /**
     * Аунтентификация пользователя
     * @param UserEntity $user
     * @return UserId|null
     */
    public function authenticate(UserEntity $user): ?UserId;

    /**
     * Создание токена для работы с апи
     * @param UserId $userId
     * @return string|null
     */
    public function createApiToken(UserId $userId): ?string;

    /**
     * Создать нового пользователя
     * @param UserEntity $userEntity
     * @return UserId
     */
    public function insert(UserEntity $userEntity): UserId;

    /**
     * Обновить пользователя
     * @param UserEntity $userEntity
     * @return void
     */
    public function update(UserEntity $userEntity): void;
}