<?php

namespace Modules\User\Domain\Builder;

use InvalidArgumentException;
use Modules\User\Domain\DTO\AuthenticationDTO;
use Modules\User\Domain\DTO\RegistrationDTO;
use Modules\User\Domain\Entity\UserEntity;
use Modules\User\Domain\ValueObject\UserEmail;
use Modules\User\Domain\ValueObject\UserName;
use Modules\User\Domain\ValueObject\UserPassword;

/**
 * Класс-помощник для создания сущности домена из данных в DTO
 */
class UserBuilder
{
    /**
     * @param RegistrationDTO $registrationDTO
     * @return UserEntity
     * @throws InvalidArgumentException
     */
    public function createFromRegistrationDTO(RegistrationDTO $registrationDTO): UserEntity
    {
        return new UserEntity(
            username: new UserName($registrationDTO->getUsername()),
            email: new UserEmail($registrationDTO->getEmail()),
            password: new UserPassword($registrationDTO->getPassword()),
            createdAt: new \DateTime(),
            updatedAt: new \DateTime(),
        );
    }

    /**
     * @param AuthenticationDTO $authenticateDTO
     * @return UserEntity
     * @throws InvalidArgumentException
     */
    public function createFromAuthenticationDTO(AuthenticationDTO $authenticateDTO): UserEntity
    {
        return new UserEntity(
            email: new UserEmail($authenticateDTO->getEmail()),
            password: new UserPassword($authenticateDTO->getPassword()),
        );
    }
}