<?php

namespace Modules\User\Domain\Entity;

use DateTimeInterface;
use Modules\Base\Domain\Entity\AbstractEntity;
use Modules\User\Domain\ValueObject\UserEmail;
use Modules\User\Domain\ValueObject\UserId;
use Modules\User\Domain\ValueObject\UserIsAdmin;
use Modules\User\Domain\ValueObject\UserName;
use Modules\User\Domain\ValueObject\UserPassword;

class UserEntity extends AbstractEntity
{
    public function __construct(
        private readonly ?UserId $id = null,
        private ?UserName $username = null,
        private ?UserEmail $email = null,
        private ?UserPassword $password = null,
        private UserIsAdmin $isAdmin = new UserIsAdmin(false),
        private readonly ?DateTimeInterface $createdAt = null,
        private ?DateTimeInterface $updatedAt = null,
    ) { }

    public function getId(): ?int
    {
        return $this->id?->get();
    }

    public function getUsername(): ?string
    {
        return $this->username?->get();
    }

    public function setUsername(?UserName $username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email?->get();
    }

    public function setEmail(?UserEmail $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): ?string
    {
        return $this->password?->get();
    }

    public function setPassword(?UserPassword $password): void
    {
        $this->password = $password;
    }

    public function getIsAdmin(): bool
    {
        return $this->isAdmin?->get();
    }

    public function setIsAdmin(UserIsAdmin $isAdmin): void
    {
        $this->isAdmin = $isAdmin;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}