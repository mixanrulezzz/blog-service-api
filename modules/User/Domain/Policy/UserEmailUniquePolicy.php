<?php

namespace Modules\User\Domain\Policy;

use Modules\User\Domain\Entity\UserEntity;
use Modules\User\Domain\Exception\UserEmailNotUniqueException;
use Modules\User\Domain\Repository\IUserRepository;
use Modules\User\Domain\ValueObject\UserEmail;

/**
 * Класс-политика для проверки email пользователя на уникальность
 */
readonly class UserEmailUniquePolicy
{
    public function __construct(
        private IUserRepository $repository,
    ) {}

    /**
     * Проверить email на уникальность
     * @param UserEntity $entity
     * @return true
     * @throws UserEmailNotUniqueException
     */
    public function check(UserEntity $entity): true
    {
        $userEntity = $this->repository->getByEmail(new UserEmail($entity->getEmail()));

        if ($userEntity) {
            throw new UserEmailNotUniqueException();
        }

        return true;
    }
}