<?php

namespace Modules\User\Domain\ValueObject;

use InvalidArgumentException;

readonly class UserEmail
{
    /**
     * @param string $email
     * @throws InvalidArgumentException
     */
    public function __construct(
        protected string $email
    ) {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException('$email should be email');
        }
    }

    /**
     * @return string
     */
    public function get(): string
    {
        return $this->email;
    }
}