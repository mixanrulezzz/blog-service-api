<?php

namespace Modules\User\Domain\ValueObject;

use InvalidArgumentException;

readonly class UserName
{
    /**
     * @param string $username
     * @throws InvalidArgumentException
     */
    public function __construct(
        protected string $username
    ) {
        if (strlen($username) < 3 || strlen($username) > 255) {
            throw new InvalidArgumentException('$username should be between 3 and 255 characters');
        }
    }

    /**
     * @return string
     */
    public function get(): string
    {
        return $this->username;
    }
}