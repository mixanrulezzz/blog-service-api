<?php

namespace Modules\User\Domain\ValueObject;

use InvalidArgumentException;

readonly class UserPassword
{
    /**
     * @param string $password
     * @throws InvalidArgumentException
     */
    public function __construct(
        protected string $password
    ) {
        if (strlen($password) < 6 || strlen($password) > 255) {
            throw new InvalidArgumentException('$password should be between 6 and 255 characters');
        }
    }

    /**
     * @return string
     */
    public function get(): string
    {
        return $this->password;
    }
}