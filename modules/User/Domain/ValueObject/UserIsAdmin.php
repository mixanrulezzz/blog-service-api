<?php

namespace Modules\User\Domain\ValueObject;

readonly class UserIsAdmin
{
    /**
     * @param bool $isAdmin
     */
    public function __construct(
        protected bool $isAdmin
    ) {}

    /**
     * @return string
     */
    public function get(): string
    {
        return $this->isAdmin;
    }
}