<?php

namespace Modules\User\Persistence\Provider;

use Illuminate\Support\ServiceProvider;
use Modules\User\Domain\Repository\IUserRepository;
use Modules\User\Persistence\Repository\CachedUserRepository;
use Modules\User\Persistence\Repository\EloquentUserRepository;
use Modules\User\UseCase\RegistrationUseCase;

class UserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(IUserRepository::class, CachedUserRepository::class);

        $this->app->when(CachedUserRepository::class)->needs(IUserRepository::class)->give(EloquentUserRepository::class);

        $this->app->when(RegistrationUseCase::class)->needs(IUserRepository::class)->give(EloquentUserRepository::class);
    }
}