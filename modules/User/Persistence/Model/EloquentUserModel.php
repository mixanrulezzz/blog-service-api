<?php

namespace Modules\User\Persistence\Model;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use InvalidArgumentException;
use Modules\Base\Persistence\Model\AbstractEloquentModel;
use Modules\User\Domain\Entity\UserEntity;
use Modules\User\Domain\ValueObject\UserEmail;
use Modules\User\Domain\ValueObject\UserId;
use Modules\User\Domain\ValueObject\UserName;
use Modules\User\Persistence\Eloquent\User;

class EloquentUserModel extends AbstractEloquentModel
{
    protected array $eloquentAttributesToSetMapping = [
        'id' => 'getId',
        'name' => 'getUsername',
        'email' => 'getEmail',
        'password' => 'getPassword',
        'created_at' => 'getCreatedAt',
        'updated_at' => 'getUpdatedAt',
    ];

    /**
     * Создание eloquent модели из сущности домена
     * @param UserEntity $entity
     * @return Model|User
     */
    public function fromDomain(UserEntity $entity): Model|User
    {
        return $this->prepareEloquentModel(new User(), $entity);
    }

    /**
     * @param User $user
     * @return UserEntity
     * @throws InvalidArgumentException
     */
    public function toDomain(User $user): UserEntity
    {
        return new UserEntity(
            id: $user->id ? new UserId($user->id) : null,
            username: $user->name ? new UserName($user->name) : null,
            email: $user->email ? new UserEmail($user->email) : null,
            createdAt: $user->created_at ? new CarbonImmutable($user->created_at) : null,
            updatedAt: $user->updated_at ? new CarbonImmutable($user->updated_at) : null,
        );
    }
}