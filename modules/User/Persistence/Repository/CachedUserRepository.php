<?php

namespace Modules\User\Persistence\Repository;

use Illuminate\Cache\Repository;
use Modules\User\Domain\Entity\UserEntity;
use Modules\User\Domain\Repository\IUserRepository;
use Modules\User\Domain\ValueObject\UserEmail;
use Modules\User\Domain\ValueObject\UserId;

/**
 * Репозиторий для пользователя, который использует cache и другой репозиторий для пользователя
 */
class CachedUserRepository implements IUserRepository
{
    public function __construct(
        private readonly IUserRepository $userRepository,
        private readonly Repository $cache,
    ) {}

    /**
     * @inheritDoc
     */
    public function getById(UserId $userId): ?UserEntity
    {
        return $this->cache->remember("user.getById.{$userId->get()}", 3600, fn() => $this->userRepository->getById($userId));
    }

    /**
     * @inheritDoc
     */
    public function getByEmail(UserEmail $userEmail): ?UserEntity
    {
        return $this->cache->remember("user.getByEmail.{$userEmail->get()}", 3600, fn() => $this->userRepository->getByEmail($userEmail));
    }

    /**
     * @inheritDoc
     */
    public function authenticate(UserEntity $user): ?UserId
    {
        return $this->userRepository->authenticate($user);
    }

    /**
     * @inheritDoc
     */
    public function createApiToken(UserId $userId): ?string
    {
        return $this->userRepository->createApiToken($userId);
    }

    /**
     * @inheritDoc
     */
    public function insert(UserEntity $userEntity): UserId
    {
        return $this->userRepository->insert($userEntity);
    }

    /**
     * @inheritDoc
     */
    public function update(UserEntity $userEntity): void
    {
        $this->userRepository->update($userEntity);
    }
}