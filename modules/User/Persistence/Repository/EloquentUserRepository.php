<?php

namespace Modules\User\Persistence\Repository;

use Modules\User\Domain\Entity\UserEntity;
use Modules\User\Domain\Repository\IUserRepository;
use Modules\User\Domain\ValueObject\UserEmail;
use Modules\User\Domain\ValueObject\UserId;
use Modules\User\Persistence\Eloquent\User;
use Modules\User\Persistence\Model\EloquentUserModel;

/**
 * Репозиторий для пользователя, который использует eloquent модель
 */
class EloquentUserRepository implements IUserRepository
{
    public function __construct(
        private readonly EloquentUserModel $model,
    ) {}

    /**
     * @inheritDoc
     */
    public function getById(UserId $userId): ?UserEntity
    {
        $user = User::whereId($userId->get())->first();

        return $user ? $this->model->toDomain($user) : null;
    }

    /**
     * @inheritDoc
     */
    public function getByEmail(UserEmail $userEmail): ?UserEntity
    {
        $user = User::whereEmail($userEmail->get())->first();

        return $user ? $this->model->toDomain($user) : null;
    }

    /**
     * @inheritDoc
     */
    public function authenticate(UserEntity $user): ?UserId
    {
        if (auth()->attempt(['email' => $user->getEmail(), 'password' => $user->getPassword()])) {
            return new UserId(auth()->id());
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function createApiToken(UserId $userId): ?string
    {
        $user = User::whereId($userId->get())->first();

        if ($user) {
            return $user->createToken('app')->plainTextToken;
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function insert(UserEntity $userEntity): UserId
    {
        $user = $this->model->fromDomain($userEntity);

        $user->save();

        return new UserId($user->id);
    }

    /**
     * @inheritDoc
     */
    public function update(UserEntity $userEntity): void
    {
        $user = $this->model->fromDomain($userEntity);

        $user->update();
    }
}