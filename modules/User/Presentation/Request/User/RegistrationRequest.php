<?php

namespace Modules\User\Presentation\Request\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Domain\DTO\RegistrationDTO;

class RegistrationRequest extends FormRequest
{
    /**
     * Кому можно выполнять запрос
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Правила валидации
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'username' => 'required|string|min:3|max:255',
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:255',
        ];
    }

    /**
     * Создание DTO из запроса
     * @return RegistrationDTO
     */
    public function toDTO(): RegistrationDTO
    {
        $this->validated();

        return new RegistrationDTO(
            $this->get('username'),
            $this->get('email'),
            $this->get('password'),
        );
    }
}