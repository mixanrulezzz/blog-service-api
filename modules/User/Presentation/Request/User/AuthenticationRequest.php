<?php

namespace Modules\User\Presentation\Request\User;

use Illuminate\Foundation\Http\FormRequest;
use Modules\User\Domain\DTO\AuthenticationDTO;

class AuthenticationRequest extends FormRequest
{
    /**
     * Кому можно выполнять запрос
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Правила валидации
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:255',
        ];
    }

    /**
     * Создание DTO из запроса
     * @return AuthenticationDTO
     */
    public function toDTO(): AuthenticationDTO
    {
        $this->validated();

        return new AuthenticationDTO(
            $this->get('email'),
            $this->get('password'),
        );
    }
}