<?php

namespace Modules\User\Presentation\Controller\User;

use InvalidArgumentException;
use Modules\Base\Presentation\Controller\BaseController;
use Modules\User\Domain\Exception\CreateUserApiTokenException;
use Modules\User\Domain\Exception\UserEmailNotUniqueException;
use Modules\User\Presentation\Request\User\RegistrationRequest;
use Modules\User\UseCase\RegistrationUseCase;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;
use OpenApi\Attributes\Response;

#[Post(
    path: '/api/v1/users/registration',
    description: 'Регистрация нового пользователя и получение токена для него',
    summary: 'Регистрация нового пользователя',
    requestBody: new RequestBody(
        required: true,
        content: new JsonContent(
            required: ['username', 'email', 'password'],
            properties: [
                new Property(
                    property: 'username',
                    description: 'Имя нового пользователя',
                    type: 'string',
                    example: 'user',
                ),
                new Property(
                    property: 'email',
                    description: 'Email нового пользователя',
                    type: 'string',
                    example: 'user@user.com',
                ),
                new Property(
                    property: 'password',
                    description: 'Пароль нового пользователя',
                    type: 'string',
                    example: '12345678',
                ),
            ],
            example: [
                'username' => 'user',
                'email' => 'user@user.com',
                'password' => '12345678',
            ],
        ),
    ),
    tags: ['Auth'],
    responses: [
        new Response(
            response: 200,
            description: 'Успешный ответ',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'token',
                        description: 'Токен пользователя',
                        type: 'string',
                        example: '3|i8BVm3FhN52K8XfKDP6FCDFJOSMQbwVpLlzIO3uka65fba18',
                    ),
                ],
            ),
        ),
        new Response(
            response: 501,
            description: 'Ошибка при создании токена для пользователя',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Ошибка при создании токена для пользователя',
                    ),
                ],
            ),
        ),
        new Response(
            response: 400,
            description: 'Неверные данные для регистрации',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Неверные данные для регистрации',
                    ),
                ],
            ),
        ),
    ],
)]
class RegistrationController extends BaseController
{
    public function __construct(
        private readonly RegistrationUseCase $useCase,
    ) {}

    public function __invoke(RegistrationRequest $request)
    {
        try {
            $apiToken = $this->useCase->__invoke($request->toDTO());
        } catch (UserEmailNotUniqueException $e) {
            return response(['message' => __('Email already in use')], 400);
        } catch (CreateUserApiTokenException $e) {
            return response(['message' => __('Create token error')], 501);
        } catch (InvalidArgumentException $e) {
            return response(['message' => $e->getMessage()], 400);
        }

        return response(['token' => $apiToken]);
    }
}