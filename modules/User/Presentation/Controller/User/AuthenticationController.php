<?php

namespace Modules\User\Presentation\Controller\User;

use InvalidArgumentException;
use Modules\Base\Presentation\Controller\BaseController;
use Modules\User\Domain\Exception\CreateUserApiTokenException;
use Modules\User\Domain\Exception\UserAuthenticationException;
use Modules\User\Presentation\Request\User\AuthenticationRequest;
use Modules\User\UseCase\AuthenticationUseCase;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;
use OpenApi\Attributes\Response;

#[Post(
    path: '/api/v1/users/auth',
    description: 'Получение токена для пользователя, для дальнейшего использования в header',
    summary: 'Получение токена для пользователя',
    requestBody: new RequestBody(
        required: true,
        content: new JsonContent(
            required: ['email', 'password'],
            properties: [
                new Property(
                    property: 'email',
                    description: 'Email зарегистрированного пользователя',
                    type: 'string',
                    example: 'user@user.com',
                ),
                new Property(
                    property: 'password',
                    description: 'Пароль зарегистрированного пользователя',
                    type: 'string',
                    example: '12345678',
                ),
            ],
            example: [
                'email' => 'user@user.com',
                'password' => '12345678',
            ]
        )
    ),
    tags: ['Auth'],
    responses: [
        new Response(
            response: 200,
            description: 'Успешный ответ',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'token',
                        description: 'Токен пользователя',
                        type: 'string',
                        example: '3|i8BVm3FhN52K8XfKDP6FCDFJOSMQbwVpLlzIO3uka65fba18',
                    ),
                ],
            ),
        ),
        new Response(
            response: 501,
            description: 'Ошибка при создании токена для пользователя',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Ошибка при создании токена для пользователя',
                    ),
                ],
            ),
        ),
        new Response(
            response: 400,
            description: 'Неверный логин или пароль',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Неверный логин или пароль',
                    ),
                ],
            ),
        ),
    ]
)]
class AuthenticationController extends BaseController
{
    public function __construct(
        private readonly AuthenticationUseCase $useCase,
    ) {}

    public function __invoke(AuthenticationRequest $request)
    {
        try {
            $apiToken = $this->useCase->__invoke($request->toDTO());
        } catch (CreateUserApiTokenException $e) {
            return response(['message' => __('Create token error')], 501);
        } catch (UserAuthenticationException $e) {
            return response(['message' => __('Wrong email or password')], 400);
        } catch (InvalidArgumentException $e) {
            return response(['message' => $e->getMessage()], 400);
        }

        return response(['token' => $apiToken]);
    }
}