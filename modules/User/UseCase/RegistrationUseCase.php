<?php

namespace Modules\User\UseCase;

use InvalidArgumentException;
use Modules\User\Domain\Builder\UserBuilder;
use Modules\User\Domain\DTO\RegistrationDTO;
use Modules\User\Domain\Exception\CreateUserApiTokenException;
use Modules\User\Domain\Exception\UserEmailNotUniqueException;
use Modules\User\Domain\Policy\UserEmailUniquePolicy;
use Modules\User\Domain\Repository\IUserRepository;

/**
 * Регистрация пользователя
 */
class RegistrationUseCase
{
    public function __construct(
        private readonly IUserRepository $repository,
        private readonly UserBuilder $builder,
        private readonly UserEmailUniquePolicy $userEmailUniquePolicy,
    ) {}

    /**
     * Регистрация пользователя
     * @param RegistrationDTO $dto
     * @return string
     * @throws UserEmailNotUniqueException
     * @throws CreateUserApiTokenException
     * @throws InvalidArgumentException
     */
    public function __invoke(RegistrationDTO $dto): string
    {
        $userEntity = $this->builder->createFromRegistrationDTO($dto);

        $this->userEmailUniquePolicy->check($userEntity);

        $userId = $this->repository->insert($userEntity);

        $token = $this->repository->createApiToken($userId);

        if (!$token) {
            throw new CreateUserApiTokenException();
        }

        return $token;
    }
}