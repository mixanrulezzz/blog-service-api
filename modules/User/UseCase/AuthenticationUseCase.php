<?php

namespace Modules\User\UseCase;

use InvalidArgumentException;
use Modules\User\Domain\Builder\UserBuilder;
use Modules\User\Domain\DTO\AuthenticationDTO;
use Modules\User\Domain\Exception\CreateUserApiTokenException;
use Modules\User\Domain\Exception\UserAuthenticationException;
use Modules\User\Domain\Repository\IUserRepository;

/**
 * Аунтентификация пользователя
 */
class AuthenticationUseCase
{
    public function __construct(
        private readonly UserBuilder $builder,
        private readonly IUserRepository $repository,
    ) {}

    /**
     * @param AuthenticationDTO $dto
     * @return string
     * @throws CreateUserApiTokenException
     * @throws UserAuthenticationException
     * @throws InvalidArgumentException
     */
    public function __invoke(AuthenticationDTO $dto): string
    {
        $userEntity = $this->builder->createFromAuthenticationDTO($dto);

        $userId = $this->repository->authenticate($userEntity);

        if (!$userId) {
            throw new UserAuthenticationException();
        }

        $token = $this->repository->createApiToken($userId);

        if (!$token) {
            throw new CreateUserApiTokenException();
        }

        return $token;
    }
}