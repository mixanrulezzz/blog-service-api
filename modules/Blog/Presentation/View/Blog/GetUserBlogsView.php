<?php

namespace Modules\Blog\Presentation\View\Blog;

use Modules\Blog\Domain\Entity\BlogEntity;

class GetUserBlogsView
{
    public function __construct(
        private GetBlogByIdView $getBlogByIdView,
    ) {}

    public function toArray(array $blogEntities): array
    {
        return collect($blogEntities)
            ->map(fn (BlogEntity $blogEntity) => $this->getBlogByIdView->toArray($blogEntity))
            ->toArray();
    }
}