<?php

namespace Modules\Blog\Presentation\View\Blog;

use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\ValueObject\AuthorId;
use Modules\Blog\Presentation\ViewModel\Blog\DateViewModel;

class GetBlogByIdView
{
    public const ID_FIELD = 'id';
    public const TITLE_FIELD = 'title';
    public const DESCRIPTION_FIELD = 'description';
    public const OWNER_ID_FIELD = 'owner_id';
    public const AUTHOR_IDS_FIELD = 'author_ids';
    public const CREATED_AT_FIELD = 'created_at';
    public const UPDATED_AT_FIELD = 'updated_at';

    public function __construct(
        private readonly DateViewModel $dateViewModel,
    ) {}

    public function toArray(BlogEntity $blogEntity)
    {
        return [
            static::ID_FIELD => $blogEntity->getId(),
            static::TITLE_FIELD => $blogEntity->getTitle(),
            static::DESCRIPTION_FIELD => $blogEntity->getDescription(),
            static::OWNER_ID_FIELD => $blogEntity->getOwnerId(),
            static::AUTHOR_IDS_FIELD => collect($blogEntity->getAuthors())
                ->map(fn (AuthorId $authorId) => $authorId->get())
                ->toArray(),
            static::CREATED_AT_FIELD => $this->dateViewModel->getFullFormatDate($blogEntity->getCreatedAt()),
            static::UPDATED_AT_FIELD => $this->dateViewModel->getFullFormatDate($blogEntity->getUpdatedAt()),
        ];
    }
}