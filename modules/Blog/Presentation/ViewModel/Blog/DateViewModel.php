<?php

namespace Modules\Blog\Presentation\ViewModel\Blog;

class DateViewModel
{
    public const FULL_FORMAT = 'd.m.Y H:i:s';

    public function getFullFormatDate(?\DateTimeInterface $date): ?string
    {
        return $date?->format(static::FULL_FORMAT);
    }
}