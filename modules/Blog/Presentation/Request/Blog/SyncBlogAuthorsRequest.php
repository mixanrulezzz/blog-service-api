<?php

namespace Modules\Blog\Presentation\Request\Blog;

use Modules\Base\Presentation\Request\FormRouteRequest;
use Modules\Blog\Domain\DTO\SyncBlogAuthorsDTO;

class SyncBlogAuthorsRequest extends FormRouteRequest
{
    /**
     * Кому можно выполнять запрос
     * @return bool
     */
    public function authorize(): bool
    {
        return \Auth::id() > 0;
    }

    /**
     * Правила валидации
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'required|int',
            'authors' => 'array',
        ];
    }

    /**
     * Создание DTO из запроса
     * @return SyncBlogAuthorsDTO
     */
    public function toDTO(): SyncBlogAuthorsDTO
    {
        $this->validated();

        return new SyncBlogAuthorsDTO(
            $this->offsetGet('id'),
            $this->get('authors'),
        );
    }
}