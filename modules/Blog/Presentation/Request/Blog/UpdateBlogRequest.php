<?php

namespace Modules\Blog\Presentation\Request\Blog;

use Modules\Base\Presentation\Request\FormRouteRequest;
use Modules\Blog\Domain\DTO\UpdateBlogDTO;

class UpdateBlogRequest extends FormRouteRequest
{
    /**
     * Кому можно выполнять запрос
     * @return bool
     */
    public function authorize(): bool
    {
        return \Auth::id() > 0;
    }

    /**
     * Правила валидации
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'required|int',
            'title' => 'sometimes|string|min:3|max:255',
            'description' => 'sometimes|string|min:3',
        ];
    }

    public function toDTO(): UpdateBlogDTO
    {
        $this->validated();

        return new UpdateBlogDTO(
            $this->offsetGet('id'),
            $this->get('title'),
            $this->get('description'),
        );
    }
}