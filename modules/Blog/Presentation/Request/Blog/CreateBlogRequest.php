<?php

namespace Modules\Blog\Presentation\Request\Blog;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Blog\Domain\DTO\CreateBlogDTO;

class CreateBlogRequest extends FormRequest
{
    /**
     * Кому можно выполнять запрос
     * @return bool
     */
    public function authorize(): bool
    {
        return \Auth::id() > 0;
    }

    /**
     * Правила валидации
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'title' => 'required|string|min:3|max:255',
            'description' => 'required|string|min:3',
        ];
    }

    /**
     * Создание DTO из запроса
     * @return CreateBlogDTO
     */
    public function toDTO(): CreateBlogDTO
    {
        $this->validated();

        return new CreateBlogDTO(
            $this->get('title'),
            $this->get('description'),
            \Auth::id()
        );
    }
}