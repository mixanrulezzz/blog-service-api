<?php

namespace Modules\Blog\Presentation\Controller\Blog;

use Modules\Base\Presentation\Controller\BaseController;
use Modules\Blog\Presentation\Request\Blog\CreateBlogRequest;
use Modules\Blog\UseCase\CreateBlogUseCase;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Post;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;
use OpenApi\Attributes\Response;

/**
 * Контроллер для создания блога
 */
#[Post(
    path: '/api/v1/blogs',
    description: 'Создание блога',
    summary: 'Создание блога',
    security: [['token'=>[]]],
    requestBody: new RequestBody(
        required: true,
        content: new JsonContent(
            required: ['title', 'description'],
            properties: [
                new Property(
                    property: 'title',
                    description: 'Заголовок блога',
                    type: 'string',
                    example: 'New blog title',
                ),
                new Property(
                    property: 'description',
                    description: 'Описание блога',
                    type: 'string',
                    example: 'Blog description',
                ),
            ],
            example: [
                'title' => 'New blog title',
                'description' => 'New blog description',
            ],
        ),
    ),
    tags: ['Blog'],
    responses: [
        new Response(
            response: 200,
            description: 'Успешный ответ',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'id',
                        description: 'ID нового блога',
                        type: 'integer',
                        example: 4,
                    ),
                ],
            ),
        ),
        new Response(
            response: 401,
            description: 'Пользователь не авторизован',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Unauthenticated.',
                    ),
                ],
            ),
        ),
        new Response(
            response: 400,
            description: 'Неверные данные для создания блога',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: '$title should be between 3 and 255 characters',
                    ),
                ],
            ),
        ),
    ],
)]
class CreateBlogController extends BaseController
{
    public function __construct(
        protected readonly CreateBlogUseCase $useCase,
    ) {}

    public function __invoke(CreateBlogRequest $request)
    {
        try {
            $blogId = $this->useCase->__invoke($request->toDTO());
        } catch (\InvalidArgumentException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return response(['id' => $blogId->get()]);
    }
}