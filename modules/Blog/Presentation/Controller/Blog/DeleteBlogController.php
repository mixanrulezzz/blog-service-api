<?php

namespace Modules\Blog\Presentation\Controller\Blog;

use Modules\Base\Domain\Exception\NoPermissionException;
use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Blog\UseCase\DeleteBlogUseCase;
use OpenApi\Attributes\Delete;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use OpenApi\Attributes\Schema;

#[Delete(
    path: '/api/v1/blogs/{id}',
    description: 'Удалить блог',
    summary: 'Удалить блог',
    security: [['token'=>[]]],
    tags: ['Blog'],
    parameters: [
        new Parameter(
            name: 'id',
            description: 'ID блога',
            in: 'path',
            required: true,
            schema: new Schema(type: 'integer'),
            example: 1,
        ),
    ],
    responses: [
        new Response(
            response: 200,
            description: 'Успешный ответ',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'success',
                        description: 'Состояние',
                        type: 'boolean',
                        example: true,
                    ),
                ],
            ),
        ),
        new Response(
            response: 401,
            description: 'Не авторизован',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Unauthenticated.',
                    ),
                ],
            ),
        ),
        new Response(
            response: 404,
            description: 'Блог не найден',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Blog is not found.',
                    ),
                ],
            ),
        ),
        new Response(
            response: 403,
            description: 'Нет нужных прав',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'No Permission.',
                    ),
                ],
            ),
        ),
        new Response(
            response: 500,
            description: 'Ошибка на сервере',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Server Error.',
                    ),
                ],
            ),
        ),
    ],
)]
class DeleteBlogController
{
    public function __construct(
        private readonly DeleteBlogUseCase $useCase,
    ) {}

    public function __invoke(int $id)
    {
        try {
            $this->useCase->__invoke($id);
        } catch (NotFoundException $e) {
            return response(['message' => $e->getMessage()], 404);
        } catch (NoPermissionException $e) {
            return response(['message' => $e->getMessage()], 403);
        } catch (\Exception $e) {
            \Log::error($e->getMessage(), [
                'trace' => $e->getTrace(),
                'controller' => __CLASS__,
                'id' => $id,
            ]);
            return response(['message' => 'Server Error'], 500);
        }

        return response(['success' => true], 200);
    }
}