<?php

namespace Modules\Blog\Presentation\Controller\Blog;

use Illuminate\Http\JsonResponse;
use InvalidArgumentException;
use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Base\Presentation\Controller\BaseController;
use Modules\Blog\Presentation\View\Blog\GetUserBlogsView;
use Modules\Blog\UseCase\GetUserBlogsUseCase;
use OpenApi\Attributes\Get;
use OpenApi\Attributes\Items;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\Response;
use OpenApi\Attributes\Schema;

/**
 * Контроллер для поиска блогов для конкретного пользователя
 */
#[Get(
    path: '/api/v1/users/{id}/blogs',
    description: 'Получение блогов пользователя по его ID',
    summary: 'Получение блогов пользователя по его ID',
    tags: ['Blog', 'User'],
    parameters: [
        new Parameter(
            name: 'id',
            description: 'ID пользователя',
            in: 'path',
            required: true,
            schema: new Schema(type: 'integer'),
            example: 1,
        ),
    ],
    responses: [
        new Response(
            response: 200,
            description: 'Успешный ответ',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'blogs',
                        description: 'Список блогов',
                        type: 'array',
                        items: new Items(
                            properties: [
                                new Property(
                                    property: 'id',
                                    description: 'ID Блога',
                                    type: 'integer',
                                    example: 1,
                                ),
                                new Property(
                                    property: 'title',
                                    description: 'Заголовок',
                                    type: 'string',
                                    example: 'Title',
                                ),
                                new Property(
                                    property: 'description',
                                    description: 'Описание блога',
                                    type: 'string',
                                    example: 'Description',
                                ),
                                new Property(
                                    property: 'owner_id',
                                    description: 'ID пользователя, который создал блог',
                                    type: 'integer',
                                    example: 1,
                                ),
                                new Property(
                                    property: 'author_ids',
                                    description: 'ID пользователей, которые имеют право писать посты в блог',
                                    type: 'array',
                                    items: new Items(
                                        type: 'integer',
                                        example: 1,
                                    ),
                                    example: [1, 2],
                                ),
                                new Property(
                                    property: 'created_at',
                                    description: 'Дата создания блога в фомате d.m.Y H:i:s',
                                    type: 'string',
                                    example: '14.05.2024 19:22:56',
                                ),
                                new Property(
                                    property: 'updated_at',
                                    description: 'Дата последнего обновления блога в фомате d.m.Y H:i:s',
                                    type: 'string',
                                    example: '14.05.2024 19:22:56',
                                ),
                            ],
                        ),
                    ),
                ],
            ),
        ),
        new Response(
            response: 404,
            description: 'Пользователь не найден',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Пользователь не найден',
                    ),
                ],
            ),
        ),
        new Response(
            response: 400,
            description: 'Неверные данные для поиска по пользователю',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Неверные данные для поиска по пользователю',
                    ),
                ],
            ),
        ),
    ],
)]
class GetUserBlogsController extends BaseController
{
    /**
     * @param GetUserBlogsUseCase $useCase
     * @param GetUserBlogsView $view
     */
    public function __construct(
        private readonly GetUserBlogsUseCase $useCase,
        private readonly GetUserBlogsView    $view,
    ) {}

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function __invoke(int $id): JsonResponse
    {
        try {
            $blogs = $this->useCase->__invoke($id);
        } catch (NotFoundException $e) {
            return response()->json(['message' => $e->getMessage()], 404);
        } catch (InvalidArgumentException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return response()->json(['blogs' => $this->view->toArray($blogs)]);
    }
}