<?php

namespace Modules\Blog\Presentation\Controller\Blog;

use Modules\Base\Domain\Exception\NoPermissionException;
use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Base\Presentation\Controller\BaseController;
use Modules\Blog\Presentation\Request\Blog\UpdateBlogRequest;
use Modules\Blog\UseCase\UpdateBlogUseCase;
use OpenApi\Attributes\JsonContent;
use OpenApi\Attributes\Parameter;
use OpenApi\Attributes\Patch;
use OpenApi\Attributes\Property;
use OpenApi\Attributes\RequestBody;
use OpenApi\Attributes\Response;
use OpenApi\Attributes\Schema;

/**
 * Контроллер для обновления блога
 */
#[Patch(
    path: '/api/v1/blogs/{id}',
    description: 'Обновить блог',
    summary: 'Обновить блог',
    security: [['token'=>[]]],
    requestBody: new RequestBody(
        required: true,
        content: new JsonContent(
            required: ['title', 'description'],
            properties: [
                new Property(
                    property: 'title',
                    description: 'Заголовок блога',
                    type: 'string',
                    example: 'Blog title',
                ),
                new Property(
                    property: 'description',
                    description: 'Описание блога',
                    type: 'string',
                    example: 'Blog description',
                ),
            ],
            example: [
                'title' => 'Blog title',
                'description' => 'Blog description',
            ],
        ),
    ),
    tags: ['Blog'],
    parameters: [
        new Parameter(
            name: 'id',
            description: 'ID блога',
            in: 'path',
            required: true,
            schema: new Schema(type: 'integer'),
            example: 1,
        ),
    ],
    responses: [
        new Response(
            response: 200,
            description: 'Успешный ответ',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'success',
                        description: 'Состояние',
                        type: 'boolean',
                        example: true,
                    ),
                ],
            ),
        ),
        new Response(
            response: 422,
            description: 'Ошибка валидации данных',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'The authors field must be an array.',
                    ),
                    new Property(
                        property: 'errors',
                        description: 'Список ошибок для каждого поля',
                        type: 'object',
                        example: ['authors' => ['The authors field must be an array.']],
                    ),
                ],
            ),
        ),
        new Response(
            response: 401,
            description: 'Не авторизован',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Unauthenticated.',
                    ),
                ],
            ),
        ),
        new Response(
            response: 404,
            description: 'Блог не найден',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Blog is not found.',
                    ),
                ],
            ),
        ),
        new Response(
            response: 403,
            description: 'Нет нужных прав',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'No Permission.',
                    ),
                ],
            ),
        ),
        new Response(
            response: 500,
            description: 'Ошибка на сервере',
            content: new JsonContent(
                properties: [
                    new Property(
                        property: 'message',
                        description: 'Сообщение об ошибке',
                        type: 'string',
                        example: 'Server Error.',
                    ),
                ],
            ),
        ),
    ],
)]
class UpdateBlogController extends BaseController
{
    public function __construct(
        private UpdateBlogUseCase $useCase,
    ) {}

    public function __invoke(int $id, UpdateBlogRequest $request)
    {
        try {
            $this->useCase->__invoke($request->toDTO());
        } catch (NotFoundException $e) {
            return response(['message' => $e->getMessage()], 404);
        } catch (NoPermissionException $e) {
            return response(['message' => $e->getMessage()], 403);
        } catch (\Exception $e) {
            \Log::error($e->getMessage(), [
                'trace' => $e->getTrace(),
                'controller' => __CLASS__,
                'id' => $id,
                'request' => $request->toArray(),
            ]);
            return response(['message' => 'Server Error.'], 500);
        }

        return response(['success' => true], 200);
    }
}