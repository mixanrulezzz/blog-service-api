<?php

namespace Modules\Blog\Persistence\Provider;

use Illuminate\Support\ServiceProvider;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\Repository\ICurrentUserRepository;
use Modules\Blog\Persistence\Repository\CachedBlogRepository;
use Modules\Blog\Persistence\Repository\EloquentBlogRepository;
use Modules\Blog\Persistence\Repository\LaravelCurrentUserRepository;

class BlogServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->app->bind(IBlogRepository::class, CachedBlogRepository::class);

        $this->app->when(CachedBlogRepository::class)->needs(IBlogRepository::class)->give(EloquentBlogRepository::class);

        $this->app->bind(ICurrentUserRepository::class, LaravelCurrentUserRepository::class);
    }
}