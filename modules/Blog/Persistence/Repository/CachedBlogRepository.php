<?php

namespace Modules\Blog\Persistence\Repository;

use Illuminate\Cache\Repository;
use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\ValueObject\BlogId;
use Modules\Blog\Domain\ValueObject\OwnerId;
use Modules\Blog\Domain\ValueObject\UserId;
use Modules\Blog\Persistence\Provider\BlogServiceProvider;

/**
 * Репозиторий для работы с закешированной сущностью блога
 */
readonly class CachedBlogRepository implements IBlogRepository
{
    /**
     * @param IBlogRepository $repository
     * @param Repository $cache
     */
    public function __construct(
        private IBlogRepository $repository, /** @see BlogServiceProvider */
        private Repository $cache
    ) {}

    /**
     * Получить блог по ID блога
     * @param BlogId $id
     * @return BlogEntity
     */
    public function getById(BlogId $id): ?BlogEntity
    {
        return $this->cache->remember("blog.getById.{$id->get()}", 3600, fn() => $this->repository->getById($id));
    }

    /**
     * Получить список блогов для пользователя (автор и создатель) по ID пользователя
     * @param UserId $id
     * @return array
     */
    public function getByUserId(UserId $id): array
    {
        return $this->cache->remember("blog.getByUserId.{$id->get()}", 3600, fn() => $this->repository->getByUserId($id));
    }

    /**
     * Сохранить блог
     * @param BlogEntity $blog
     * @return BlogId
     */
    public function insert(BlogEntity $blog): BlogId
    {
        return $this->repository->insert($blog);
    }

    /**
     * Обновить блог
     * @param BlogEntity $blog
     * @return void
     */
    public function update(BlogEntity $blog): void
    {
        $this->repository->update($blog);
    }

    /**
     * Обновить блог
     * @param BlogId $id
     * @return void
     */
    public function delete(BlogId $id): void
    {
        $this->repository->delete($id);
    }

    /**
     * Обновить список авторов
     * @param BlogEntity $blog
     * @return void
     */
    public function syncAuthors(BlogEntity $blog): void
    {
        $this->repository->syncAuthors($blog);
    }
}