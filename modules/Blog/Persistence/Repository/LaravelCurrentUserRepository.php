<?php

namespace Modules\Blog\Persistence\Repository;

use Illuminate\Auth\AuthManager;
use Modules\Blog\Domain\Repository\ICurrentUserRepository;

readonly class LaravelCurrentUserRepository implements ICurrentUserRepository
{
    public function __construct(
        private AuthManager $manager,
    ) {}

    /**
     * @inheritDoc
     */
    public function getId(): ?int
    {
        return (int)$this->manager->id();
    }

    /**
     * @inheritDoc
     */
    public function isAdmin(): bool
    {
        return false; // todo admin logic
    }
}