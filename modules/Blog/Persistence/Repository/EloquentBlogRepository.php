<?php

namespace Modules\Blog\Persistence\Repository;

use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\ValueObject\AuthorId;
use Modules\Blog\Domain\ValueObject\BlogId;
use Modules\Blog\Domain\ValueObject\OwnerId;
use Modules\Blog\Domain\ValueObject\UserId;
use Modules\Blog\Persistence\Eloquent\Blog;
use Modules\Blog\Persistence\Model\EloquentBlogModel;

/**
 * Репозиторий для работы с сущностью блога в БД с помощью eloquent модели
 */
readonly class EloquentBlogRepository implements IBlogRepository
{
    /**
     * @param EloquentBlogModel $eloquentBlogModel
     */
    public function __construct(
        protected EloquentBlogModel $eloquentBlogModel
    ) {}

    /**
     * Получить блог по ID блога
     * @param BlogId $id
     * @return BlogEntity|null
     */
    public function getById(BlogId $id): ?BlogEntity
    {
        $blog = Blog::where('id', $id->get())->first();
        return $this->eloquentBlogModel->toDomain($blog);
    }

    /**
     * Получить список блогов для пользователя (автор и создатель) по ID пользователя
     * @param UserId $id
     * @return array
     */
    public function getByUserId(UserId $id): array
    {
        return Blog::where('owner_id', $id->get())
            ->orWhere('authors.author_id', $id->get())
            ->get()
            ->map(fn (Blog $blog) => $this->eloquentBlogModel->toDomain($blog))
            ->toArray() ?? [];
    }

    /**
     * Сохранить блог
     * @param BlogEntity $blog
     * @return BlogId
     */
    public function insert(BlogEntity $blog): BlogId
    {
        $eloquentBlog = $this->eloquentBlogModel->fromDomain($blog);
        $eloquentBlog->save();

        return new BlogId($eloquentBlog->id);
    }

    /**
     * Обновить блог
     * @param BlogEntity $blog
     * @return void
     * @throws NotFoundException
     */
    public function update(BlogEntity $blog): void
    {
        $eloquentBlog = Blog::where('id', $blog->getId())->first();

        if (is_null($eloquentBlog)) {
            throw new NotFoundException(__('Blog not found'));
        }

        $eloquentBlog = $this->eloquentBlogModel->fromDomain($blog, $eloquentBlog);
        $eloquentBlog->save();
    }

    /**
     * Удалить блог
     * @param BlogId $id
     * @return void
     * @throws NotFoundException
     */
    public function delete(BlogId $id): void
    {
        $eloquentBlog = Blog::where('id', $id->get())->first();

        if (is_null($eloquentBlog)) {
            throw new NotFoundException(__('Blog not found'));
        }

        $eloquentBlog->delete();
    }

    /**
     * Обновить список авторов
     * @param BlogEntity $blog
     * @return void
     */
    public function syncAuthors(BlogEntity $blog): void
    {
        $authorIds = array_map(
            fn(AuthorId $authorId) => $authorId->get(),
            $blog->getAuthors()
        );

        $blog = Blog::where('id', $blog->getId())->first();

        $blog->authors()->sync($authorIds);
    }
}