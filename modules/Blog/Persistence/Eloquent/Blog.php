<?php

namespace Modules\Blog\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Modules\User\Persistence\Eloquent\User;


class Blog extends Model
{
    /**
     * Получить создателя блога
     * @return HasOne
     */
    public function owner(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'owner_id');
    }

    /**
     * Получить авторов блога
     * @return BelongsToMany
     */
    public function authors(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'authors_blogs', 'blog_id', 'author_id');
    }
}