<?php

namespace Modules\Blog\Persistence\Model;

use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Model;
use Modules\Base\Persistence\Model\AbstractEloquentModel;
use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\ValueObject\BlogDescription;
use Modules\Blog\Domain\ValueObject\BlogId;
use Modules\Blog\Domain\ValueObject\BlogTitle;
use Modules\Blog\Domain\ValueObject\OwnerId;
use Modules\Blog\Persistence\Eloquent\Blog;

class EloquentBlogModel extends AbstractEloquentModel
{
    protected array $eloquentAttributesToSetMapping = [
        'id' => 'getId',
        'title' => 'getTitle',
        'description' => 'getDescription',
        'created_at' => 'getCreatedAt',
        'updated_at' => 'getUpdatedAt',
        'owner_id' => 'getOwnerId',
    ];

    /**
     * Создание eloquent модели из сущности домена
     * @param BlogEntity $entity
     * @param Blog|null $blog
     * @return Model|Blog
     */
    public function fromDomain(BlogEntity $entity, ?Blog $blog = null): Model|Blog
    {
        return $this->prepareEloquentModel($blog ?: new Blog(), $entity);
    }

    /**
     * Создание сущности домена из eloquent модели
     * @param Blog|null $blog
     * @return BlogEntity|null
     */
    public function toDomain(?Blog $blog): ?BlogEntity
    {
        return $blog ? new BlogEntity(
            id: $blog->id ? new BlogId($blog->id) : null,
            title: $blog->title ? new BlogTitle($blog->title) : null,
            description: $blog->description ? new BlogDescription($blog->description) : null,
            createdAt: $blog->created_at ? new CarbonImmutable($blog->created_at) : null,
            updatedAt: $blog->updated_at ? new CarbonImmutable($blog->updated_at) : null,
            ownerId: $blog->owner_id ? new OwnerId($blog->owner_id) : null,
        ) : null;
    }
}