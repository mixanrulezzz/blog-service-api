<?php

namespace Modules\Blog\UseCase;

use InvalidArgumentException;
use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\ValueObject\UserId;

/**
 * Класс для поиска блогов, которыми владеет пользователь
 */
class GetUserBlogsUseCase
{
    /**
     * @param IBlogRepository $repository
     */
    public function __construct(
        private IBlogRepository $repository
    ) {}

    /**
     * @param int $id
     * @return array
     * @throws NotFoundException
     * @throws InvalidArgumentException
     */
    public function __invoke(int $id): array
    {
        $blogs = $this->repository->getByUserId(new UserId($id));

        if (count($blogs) === 0) {
            throw new NotFoundException(__('User does not have any blogs.'));
        }

        return $blogs;
    }
}