<?php

namespace Modules\Blog\UseCase;

use Modules\Blog\Domain\Builder\BlogBuilder;
use Modules\Blog\Domain\DTO\CreateBlogDTO;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\ValueObject\BlogId;

/**
 * Создание блога
 */
class CreateBlogUseCase
{
    /**
     * @param IBlogRepository $repository
     * @param BlogBuilder $builder
     */
    public function __construct(
        private IBlogRepository $repository,
        private BlogBuilder $builder,
    ) {}

    /**
     * @param CreateBlogDTO $dto
     * @return BlogId
     * @throws \InvalidArgumentException
     */
    public function __invoke(CreateBlogDTO $dto): BlogId
    {
        $blogEntity = $this->builder->fromCreateBlogDTO($dto);

        return $this->repository->insert($blogEntity);
    }
}