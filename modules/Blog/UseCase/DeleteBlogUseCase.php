<?php

namespace Modules\Blog\UseCase;

use Modules\Base\Domain\Exception\NoPermissionException;
use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\Policy\UserHasRightsToChangeBlogPolicy;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\ValueObject\BlogId;

readonly class DeleteBlogUseCase
{
    public function __construct(
        private IBlogRepository $repository,
        private UserHasRightsToChangeBlogPolicy $policy,
    ) {}

    /**
     * @param int $id
     * @return void
     * @throws NotFoundException
     * @throws NoPermissionException
     */
    public function __invoke(int $id)
    {
        $blogEntity = new BlogEntity(new BlogId($id));

        $this->policy->check($blogEntity);

        $this->repository->delete(new BlogId($id));
    }
}