<?php

namespace Modules\Blog\UseCase;

use InvalidArgumentException;
use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\ValueObject\BlogId;

/**
 * Класс для поиска блога по его ID
 */
class GetBlogByIdUseCase
{
    /**
     * @param IBlogRepository $repository
     */
    public function __construct(
        private IBlogRepository $repository
    ) {}

    /**
     * @param int $id
     * @return BlogEntity
     * @throws NotFoundException
     * @throws InvalidArgumentException
     */
    public function __invoke(int $id): BlogEntity
    {
        $blog = $this->repository->getById(new BlogId($id));

        if (is_null($blog)) {
            throw new NotFoundException(__('Blog not found'));
        }

        return $this->repository->getById(new BlogId($id));
    }
}