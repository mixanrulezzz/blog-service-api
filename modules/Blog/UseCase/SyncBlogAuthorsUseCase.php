<?php

namespace Modules\Blog\UseCase;

use Modules\Base\Domain\Exception\NoPermissionException;
use Modules\Blog\Domain\Builder\BlogBuilder;
use Modules\Blog\Domain\DTO\SyncBlogAuthorsDTO;
use Modules\Blog\Domain\Policy\UserHasRightsToChangeBlogPolicy;
use Modules\Blog\Domain\Repository\IBlogRepository;

class SyncBlogAuthorsUseCase
{
    public function __construct(
        private IBlogRepository $repository,
        private BlogBuilder $builder,
        private UserHasRightsToChangeBlogPolicy $policy,
    ) {}

    /**
     * @param SyncBlogAuthorsDTO $dto
     * @return void
     * @throws NoPermissionException
     */
    public function __invoke(SyncBlogAuthorsDTO $dto): void
    {
        $blogEntity = $this->builder->fromSyncBlogAuthorsDTO($dto);

        $this->policy->check($blogEntity);

        $this->repository->syncAuthors($blogEntity);
    }
}