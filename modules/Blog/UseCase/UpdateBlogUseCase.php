<?php

namespace Modules\Blog\UseCase;

use Modules\Base\Domain\Exception\NoPermissionException;
use Modules\Base\Domain\Exception\NotFoundException;
use Modules\Blog\Domain\Builder\BlogBuilder;
use Modules\Blog\Domain\DTO\UpdateBlogDTO;
use Modules\Blog\Domain\Policy\UserHasRightsToChangeBlogPolicy;
use Modules\Blog\Domain\Repository\IBlogRepository;

readonly class UpdateBlogUseCase
{
    public function __construct(
        private BlogBuilder $builder,
        private IBlogRepository $repository,
        private UserHasRightsToChangeBlogPolicy $policy,
    ) {}

    /**
     * @param UpdateBlogDTO $dto
     * @return void
     * @throws NotFoundException
     * @throws NoPermissionException
     */
    public function __invoke(UpdateBlogDTO $dto)
    {
        $blogEntity = $this->builder->fromUpdateBlogDTO($dto);

        $this->policy->check($blogEntity);

        $this->repository->update($blogEntity);
    }
}