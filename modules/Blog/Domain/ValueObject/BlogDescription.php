<?php

namespace Modules\Blog\Domain\ValueObject;

class BlogDescription
{
    public function __construct(
        protected readonly string $description
    ) {
        if (strlen($description) < 3) {
            throw new \InvalidArgumentException('$description must be at least 3 characters long');
        }
    }

    public function get(): string
    {
        return $this->description;
    }
}