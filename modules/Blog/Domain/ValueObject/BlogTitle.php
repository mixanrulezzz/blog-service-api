<?php

namespace Modules\Blog\Domain\ValueObject;

class BlogTitle
{
    public function __construct(
        protected readonly string $title
    ) {
        if (strlen($title) < 3 || strlen($title) > 255) {
            throw new \InvalidArgumentException('$title should be between 3 and 255 characters');
        }
    }

    public function get(): string
    {
        return $this->title;
    }
}