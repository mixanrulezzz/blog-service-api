<?php

namespace Modules\Blog\Domain\Builder;

use Modules\Blog\Domain\DTO\SyncBlogAuthorsDTO;
use Modules\Blog\Domain\DTO\CreateBlogDTO;
use Modules\Blog\Domain\DTO\UpdateBlogDTO;
use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\ValueObject\AuthorId;
use Modules\Blog\Domain\ValueObject\BlogDescription;
use Modules\Blog\Domain\ValueObject\BlogId;
use Modules\Blog\Domain\ValueObject\BlogTitle;
use Modules\Blog\Domain\ValueObject\OwnerId;

/**
 * Класс-помощник для создания сущности домена из данных в DTO
 */
class BlogBuilder
{
    /**
     * @param CreateBlogDTO $dto
     * @return BlogEntity
     */
    public function fromCreateBlogDTO(CreateBlogDTO $dto): BlogEntity
    {
        return new BlogEntity(
            title: new BlogTitle($dto->getTitle()),
            description: new BlogDescription($dto->getDescription()),
            createdAt: new \DateTime(),
            updatedAt: new \DateTime(),
            ownerId: new OwnerId($dto->getOwnerId()),
        );
    }

    /**
     * @param UpdateBlogDTO $dto
     * @return BlogEntity
     */
    public function fromUpdateBlogDTO(UpdateBlogDTO $dto): BlogEntity
    {
        return new BlogEntity(
            id: new BlogId($dto->getId()),
            title: $dto->getTitle() ? new BlogTitle($dto->getTitle()) : null,
            description: $dto->getDescription() ? new BlogDescription($dto->getDescription()) : null,
        );
    }

    /**
     * @param SyncBlogAuthorsDTO $dto
     * @return BlogEntity
     */
    public function fromSyncBlogAuthorsDTO(SyncBlogAuthorsDTO $dto): BlogEntity
    {
        $entity = new BlogEntity(
            id: new BlogId($dto->getBlogId()),
        );

        foreach ($dto->getAuthorIds() as $authorId) {
            $entity->addAuthor(new AuthorId($authorId));
        }

        return $entity;
    }
}