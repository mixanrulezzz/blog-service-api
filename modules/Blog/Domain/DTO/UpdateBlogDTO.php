<?php

namespace Modules\Blog\Domain\DTO;

readonly class UpdateBlogDTO
{
    public function __construct(
        private int $id,
        private ?string $title,
        private ?string $description,
    ) {}

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}