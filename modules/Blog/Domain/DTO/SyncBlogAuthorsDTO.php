<?php

namespace Modules\Blog\Domain\DTO;

class SyncBlogAuthorsDTO
{
    public function __construct(
        private int $blogId,
        private array $authorIds,
    ) {}

    public function getBlogId(): int
    {
        return $this->blogId;
    }

    public function getAuthorIds(): array
    {
        return $this->authorIds;
    }
}