<?php

namespace Modules\Blog\Domain\DTO;

class CreateBlogDTO
{
    public function __construct(
        private string $title,
        private string $description,
        private int $ownerId,
    ) {}

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }
}