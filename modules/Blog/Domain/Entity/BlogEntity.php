<?php

namespace Modules\Blog\Domain\Entity;

use DateTimeInterface;
use Modules\Base\Domain\Entity\AbstractEntity;
use Modules\Blog\Domain\ValueObject\AuthorId;
use Modules\Blog\Domain\ValueObject\BlogDescription;
use Modules\Blog\Domain\ValueObject\BlogId;
use Modules\Blog\Domain\ValueObject\BlogTitle;
use Modules\Blog\Domain\ValueObject\OwnerId;

class BlogEntity extends AbstractEntity
{
    private array $authors = [];

    public function __construct(
        private readonly ?BlogId $id = null,
        private ?BlogTitle $title = null,
        private ?BlogDescription $description = null,
        private readonly ?DateTimeInterface $createdAt = null,
        private ?DateTimeInterface $updatedAt = null,
        private readonly ?OwnerId $ownerId = null,
    ) { }

    public function getId(): ?int
    {
        return $this->id?->get();
    }

    public function getTitle(): ?string
    {
        return $this->title?->get();
    }

    public function setTitle(?BlogTitle $title): void
    {
        $this->title = $title;
    }

    public function getDescription(): ?string
    {
        return $this->description?->get();
    }

    public function setDescription(?BlogDescription $description): void
    {
        $this->description = $description;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getAuthors(): array
    {
        return $this->authors;
    }

    public function addAuthor(AuthorId $authorId): void
    {
        $this->authors[$authorId->get()] = $authorId;
    }

    public function removeAuthor(AuthorId $authorId): void
    {
        unset($this->authors[$authorId->get()]);
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId?->get();
    }
}