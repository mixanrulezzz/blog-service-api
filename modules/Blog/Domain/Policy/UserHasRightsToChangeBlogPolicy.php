<?php

namespace Modules\Blog\Domain\Policy;

use Modules\Base\Domain\Exception\NoPermissionException;
use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\Repository\IBlogRepository;
use Modules\Blog\Domain\Repository\ICurrentUserRepository;
use Modules\Blog\Domain\ValueObject\BlogId;

readonly class UserHasRightsToChangeBlogPolicy
{
    public function __construct(
        private IBlogRepository $blogRepository,
        private ICurrentUserRepository $currentUserRepository,
    ) {}

    /**
     * @param BlogEntity $entity
     * @return true
     * @throws NoPermissionException
     */
    public function check(BlogEntity $entity): true
    {
        $blog = $this->blogRepository->getById(new BlogId($entity->getId()));
        $ownerId = $blog->getOwnerId();

        if ($this->currentUserRepository->getId() != $ownerId) {
            throw new NoPermissionException();
        }

        return true;
    }
}