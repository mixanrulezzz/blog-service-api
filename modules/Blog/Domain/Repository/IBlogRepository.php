<?php

namespace Modules\Blog\Domain\Repository;

use Modules\Blog\Domain\Entity\BlogEntity;
use Modules\Blog\Domain\ValueObject\BlogId;
use Modules\Blog\Domain\ValueObject\OwnerId;
use Modules\Blog\Domain\ValueObject\UserId;

/**
 * Интерфейс для репозиториев сущности блога
 */
interface IBlogRepository
{
    /**
     * Получить блог по ID блога
     * @param BlogId $id
     * @return BlogEntity|null
     */
    public function getById(BlogId $id): ?BlogEntity;

    /**
     * Получить список блогов для пользователя (автор и создатель) по ID пользователя
     * @param UserId $id
     * @return array
     */
    public function getByUserId(UserId $id): array;

    /**
     * Сохранить блог
     * @param BlogEntity $blog
     * @return BlogId
     */
    public function insert(BlogEntity $blog): BlogId;

    /**
     * Обновить блог
     * @param BlogEntity $blog
     * @return void
     */
    public function update(BlogEntity $blog): void;

    /**
     * Удалить блог
     * @param BlogId $id
     * @return void
     */
    public function delete(BlogId $id): void;

    /**
     * Обновить список авторов
     * @param BlogEntity $blog
     * @return void
     */
    public function syncAuthors(BlogEntity $blog): void;
}