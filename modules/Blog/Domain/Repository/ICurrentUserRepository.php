<?php

namespace Modules\Blog\Domain\Repository;

interface ICurrentUserRepository
{
    /**
     * Получить ID текущего пользователя
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * Является ли текущий пользователь админом
     * @return bool
     */
    public function isAdmin(): bool;
}