<?php

namespace Modules\Base\Persistence\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Base\Domain\Entity\AbstractEntity;

abstract class AbstractEloquentModel
{
    /**
     * Массив с атрибутами eloquent модели и методами entity, из которых можно получить данные для этих атрибутов
     * @var array
     */
    protected array $eloquentAttributesToSetMapping = [];

    /**
     * Проставить атрибуты модели с помощью данных из доменной сущности
     * @param Model $model
     * @param AbstractEntity $entity
     * @return Model
     */
    protected function prepareEloquentModel(Model $model, AbstractEntity $entity): Model
    {
        foreach ($this->eloquentAttributesToSetMapping as $attrKey => $getMethod) {
            if ($this->isSetAttribute($model, $entity, $attrKey, $getMethod)) {
                $model->setAttribute($attrKey, $entity->$getMethod());
            }
        }

        return $model;
    }

    /**
     * Можно ли проставлять атрибут у модели
     * @param Model $model
     * @param AbstractEntity $entity
     * @param string $attrKey
     * @param string $getMethod
     * @return bool
     */
    protected function isSetAttribute(Model $model, AbstractEntity $entity, string $attrKey, string $getMethod): bool
    {
        return !is_null($entity->$getMethod());
    }
}