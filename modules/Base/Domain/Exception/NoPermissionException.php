<?php

namespace Modules\Base\Domain\Exception;

class NoPermissionException extends \Exception
{
    protected $message = 'No Permission.';
}