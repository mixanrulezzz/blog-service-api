<?php

namespace Modules\Base\Domain\ValueObject;

use InvalidArgumentException;

abstract class AbstractIntegerId
{
    /**
     * @param int $id
     * @throws InvalidArgumentException
     */
    public function __construct(
        protected readonly int $id
    ) {
        if ($id <= 0) {
            throw new InvalidArgumentException('$id must be a positive integer');
        }
    }

    /**
     * Получить целочисленный id
     * @return int
     */
    public function get(): int
    {
        return $this->id;
    }
}