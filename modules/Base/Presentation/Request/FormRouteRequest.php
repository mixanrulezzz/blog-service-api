<?php

namespace Modules\Base\Presentation\Request;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Класс для реквеста с параметрами из route для проверки
 */
class FormRouteRequest extends FormRequest
{
    public function validationData()
    {
        // Добавляем параметры из route в массив для валидации данных
        return array_replace_recursive(parent::validationData(), $this->route()->parameters());
    }
}