<?php

namespace Modules\Base\Presentation\Controller;

use Illuminate\Routing\Controller;
use OpenApi\Attributes\Info;
use OpenApi\Attributes\SecurityScheme;

#[Info(
    version: '0.1.0',
    description: 'Blog service API',
    title: 'Blog service API',
)]
#[SecurityScheme(
    securityScheme: 'token',
    type: 'apiKey',
    description: 'Токен для авторизации. Формат: Bearer \<token\>',
    name: 'Authorization',
    in: 'header',
    bearerFormat: 'Bearer',
)]
class BaseController extends Controller
{

}