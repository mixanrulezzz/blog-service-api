<?php

use L5Swagger\L5SwaggerServiceProvider;
use Modules\Blog\Persistence\Provider\BlogServiceProvider;
use Modules\User\Persistence\Provider\UserServiceProvider;

return [
    App\Providers\AppServiceProvider::class,
    BlogServiceProvider::class,
    UserServiceProvider::class,
    L5SwaggerServiceProvider::class,
];
