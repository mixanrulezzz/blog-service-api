<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Modules\Blog\Persistence\Eloquent\Blog;
use Modules\User\Persistence\Eloquent\User;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('authors_blogs', function (Blueprint $table) {
            $table->foreignIdFor(User::class, 'author_id')
                ->constrained('users')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->foreignIdFor(Blog::class, 'blog_id')
                ->constrained('blogs')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();

            $table->primary(['author_id', 'blog_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('authors_blogs');
    }
};
