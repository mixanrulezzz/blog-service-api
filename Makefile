pwd=$(shell pwd)
user=$(shell id -u)
group=$(shell id -g)

install:
	@docker run --rm -u "$(user):$(group)" -v "$(pwd):/var/www/html" -w /var/www/html laravelsail/php83-composer:latest composer install --ignore-platform-reqs
	@if [ ! -f .env ]; then cp .env.example .env; fi;
	@./vendor/bin/sail up -d
	@./vendor/bin/sail artisan key:generate
	@./vendor/bin/sail artisan migrate
	@./vendor/bin/sail npm install
	@./vendor/bin/sail npm run build
	@./vendor/bin/sail down