<?php

use Modules\Blog\Presentation\Controller\Blog\CreateBlogController;
use Modules\Blog\Presentation\Controller\Blog\DeleteBlogController;
use Modules\Blog\Presentation\Controller\Blog\GetBlogByIdController;
use Modules\Blog\Presentation\Controller\Blog\GetUserBlogsController;
use Modules\Blog\Presentation\Controller\Blog\SyncBlogAuthorsController;
use Modules\Blog\Presentation\Controller\Blog\UpdateBlogController;
use Modules\User\Presentation\Controller\User\AuthenticationController;
use Modules\User\Presentation\Controller\User\RegistrationController;

Route::prefix('blogs')
    ->name('blog.')
    ->group(function () {
        Route::get('{id}', GetBlogByIdController::class)
            ->whereNumber('id');

        Route::middleware('auth:sanctum')
            ->group(function () {
                Route::post('', CreateBlogController::class);

                Route::patch('{id}', UpdateBlogController::class);

                Route::delete('{id}', DeleteBlogController::class);

                Route::put('{id}/authors', SyncBlogAuthorsController::class);
            });
    });

Route::prefix('users')
    ->name('user.')
    ->group(function () {
        Route::middleware('guest:sanctum')
            ->group(function () {
            Route::post('registration', RegistrationController::class);

            Route::post('auth', AuthenticationController::class);
        });

        Route::get('{id}/blogs', GetUserBlogsController::class)
            ->whereNumber('id');
    });